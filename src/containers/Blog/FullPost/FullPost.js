import React, { Component } from 'react';
import axios from 'axios';

import './FullPost.css';

class FullPost extends Component {

    state = {
        loadedPost : null
    }


    /*
    componentDidUpdate(){
        if(this.props.id){
            if(!this.state.loadedPost || (this.state.loadedPost && this.state.loadedPost.id !== this.props.id)){
                axios.get('/posts/' + this.props.id)
                .then((response) => {
                    this.setState({
                        loadedPost : response.data
                    })
                })
            }
        }
    }
    */
    


    componentDidMount(){
        this.loadData();
    }

    componentDidUpdate(){
        this.loadData();
    }

    async loadData() {
        const postId = this.props.match.params.id;

        
        if(postId){
            
            try{
                if(this.state.loadedPost && this.state.loadedPost.id !== postId){
                    const postRes = await axios.get('/posts/' + postId);
                    
            
                    this.setState({
                        loadedPost : postRes.data
                    })         
                }
            }catch(error){
                console.log(error);
            }
        }
    } 


    deleteDataHandler2 = () => {
        if(this.state.loadedPost){
            axios.delete('/posts/' + this.state.loadedPost.id)
            .then((response) => {
                //console.log(response);
            }).catch((error) => {
                //console.log("Operation error: " + error);
            })
        }
    }


    deleteDataHandler = async () => {

        //console.log(this.state.loadedPost);
        if(this.state.loadedPost){
            try{
                const response = await axios.delete('/posts/' + this.state.loadedPost.id)
                console.log(response);
            }catch(error){
                //console.log("Operation error: " + error);
            }
        }
    }



    render () {
        let post = <p style={{textAlign: 'center'}}>Please select a Post!</p>;

        if(this.props.match.params.id){
            post = <p style={{textAlign: 'center'}}>Loading...</p>;

        }
        if(this.state.loadedPost)
        {
            post = (
                <div className="FullPost">
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                    <div className="Edit">
                        <button className="Delete" onClick={this.deleteDataHandler}>Delete</button>
                    </div>
                </div>
    
            );
        }
        return post;
    }
}

export default FullPost;