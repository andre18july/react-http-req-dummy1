import React, { Component } from 'react';
import Posts from './Posts/Posts';
import { Route , NavLink, Switch, Redirect } from 'react-router-dom';
import './Blog.css';

//import NewPost from './NewPost/NewPost';

import asyncComponent from '../../hoc/AsyncComponent/asyncComponent';
const AsyncNewPost = asyncComponent(() => {
    return import('./NewPost/NewPost');
}); 



class Blog extends Component {

    state = {
        auth: true
    }


    render () {

        return (
            <div className="Blog">
                <header>
                    <nav>
                        <ul>
                            <li><NavLink 
                                to="/posts/" 
                                exact
                                activeStyle={{
                                    color: '#ff0000',
                                    textDecoration: 'underline'
                                }}
                            >Posts</NavLink></li>
                            <li><NavLink to="/new-post" exact>New Post</NavLink></li>
                        </ul>
                    </nav>
                </header>

                <Switch>

                    {/*<Route path="/" exact render={() => <Posts/>}/>*/}
                    {this.state.auth ? <Route path="/new-post" component={AsyncNewPost}/> : null}
                    <Route path="/posts" component={Posts}/>
                    <Route render = {() => <h1>Not found!!!</h1>} /> 
                    
                    {/*<Redirect from="/" to="/posts/"/>*/}
                    {/*<Route path="/" component={Posts}/>*/}
                    
                </Switch>

            </div>
        );
    }
}

export default Blog;